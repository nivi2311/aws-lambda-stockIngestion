import json
import boto3
import sys
import yfinance as yf
import pandas as pd
import numpy as np

import time
import random
import datetime

def myconverter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()

stocks = ['MSFT','MVIS', 'GOOG', 'SPOT', 'INO', 'OCGN', 'ABML', 'RLLCF', 'JNJ', 'PSFE']

# Your goal is to get per-hour stock price data for a time range for the ten stocks specified in the doc. 
# Further, you should call the static info api for the stocks to get their current 52WeekHigh and 52WeekLow values.
# You should craft individual data records with information about the stockid, price, price timestamp, 52WeekHigh and 52WeekLow values and push them individually on the Kinesis stream

kinesis = boto3.client('kinesis', region_name = "us-east-1") #Modify this line of code according to your requirement.

today = datetime.date.today()
yesterday = datetime.date.today() - datetime.timedelta(1)

## Add code to pull the data for the stocks specified in the doc

for stock in stocks:
    data = yf.download(stock, start="2021-09-13", end="2021-09-17", interval = '1h')
    print("\n********************",stock,"********************\n")
    stock_dict = {}
    datetime_list = data.index.values
    datetime_list_utc = []
    for day in datetime_list:
        ts = (day - np.datetime64('1970-01-01T00:00:00Z')) / np.timedelta64(1, 's')
        #datetime.utcfromtimestamp(ts)
        datetime_list_utc.append(datetime.datetime.utcfromtimestamp(ts))
    
    ## Add additional code to call 'info' API to get 52WeekHigh and 52WeekLow

    info = yf.Ticker(stock).info
    i=0
    for index, row in data.iterrows():
        stock_dict['stockID'] = stock
        stock_dict['Closing_price'] = row['Close']
        stock_dict['price_timestamp'] = datetime_list_utc[i]
        stock_dict['52WeekHigh'] = info.get('fiftyTwoWeekHigh')
        stock_dict['52WeekLow'] = info.get('fiftyTwoWeekLow')
        print(stock_dict)
        # loop over each of the data rows received 
        kinesis_record = json.dumps(stock_dict, indent=2, default=myconverter).encode('utf-8')#{k: str(v).encode("utf-8") for k,v in stock_dict.items()}
        
        ## Add your code here to push data records to Kinesis stream.
        
        response = kinesis.put_record(
            StreamName='gl-stock-data-stream',
            Data=kinesis_record,
            PartitionKey=str(1)
        )
        print(response)
        i=i+1


#Code for retrieving records from kinesis. For testing purpose

# ShardIterator = kinesis.get_shard_iterator(
#     StreamName='gl-stock-data-stream',
#     ShardId='shardId-000000000000',
#     ShardIteratorType='AFTER_SEQUENCE_NUMBER',
#      StartingSequenceNumber='49622009631619723361166974671822365917985482068990099458'
# )

# response = kinesis.get_records(
#     ShardIterator=ShardIterator['ShardIterator'],
#     Limit=200
# )

# print("\n*********************** Kinesis Fetch Response ***********************\n")
# print(response)
