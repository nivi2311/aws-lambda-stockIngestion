from __future__ import print_function
import base64
import json
import boto3
from decimal import Decimal
import datetime
from boto3.dynamodb.conditions import Key

def lambda_handler(event, context):
    dynamodb_res = boto3.resource('dynamodb', region_name='us-east-1')
    table = dynamodb_res.Table('stock-data-alerts')
    client = boto3.client('sns', region_name='us-east-1')
    topic_arn = "arn:aws:sns:us-east-1:810914808867:gl-stock-data"

    for record in event['Records']:
        #kinesis data is base64 encioded so decode here
        payload=base64.b64decode(record["kinesis"]["data"])
        print("Decoded payload: " + str(payload))
        stock_dict = json.loads(payload, parse_float=Decimal)
        dt = datetime.datetime.strptime(stock_dict['price_timestamp'], '%Y-%m-%d %H:%M:%S')
        stock_dict['date'] = str(dt.day) + "-" + str(dt.year)
        if stock_dict['Closing_price']>=(Decimal(0.8)*stock_dict['52WeekHigh']) or stock_dict['Closing_price']<=(Decimal(1.2)*stock_dict['52WeekLow']):
            print("closing_price: :" + str(stock_dict['Closing_price']))
            db_response = table.query(
                KeyConditionExpression=Key('date').eq(stock_dict['date']) & Key('stockID').eq(stock_dict['stockID'])
            )
            if len(db_response['Items']) == 0:
                response = table.put_item(Item=stock_dict)
                print(response)
                client.publish(TopicArn=topic_arn, Message=str(stock_dict), Subject="stockdataPOI")
                print("Alerted")
           